/// <reference path="../typings/node/node.d.ts"/>
'use strict';

var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var path = require('path');
var clientPath = path.normalize(__dirname + '/..') + '/client';

var app = express();

// Register Express middlewares
app.use(express.static(clientPath));
app.use(morgan('dev'));
app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

// Register todoController routes under /api
app.use('/api', require('./controllers/todoController'));

// SPA start page (angular will handle other routes frontend)
app.get("/*", function (request, response) {
    var options = {
        root: clientPath
    };
    response.sendFile('index.html', options);
});

// Start web server
app.listen(3000);

console.log("Listening on http://localhost:3000/");