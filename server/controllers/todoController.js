(function () {
    'use strict';

    var mongojs = require('mongojs');
    var db = mongojs('mongodb://localhost/meantodo', ['todos']);

    var router = require('express').Router();

    router.get("/todo",
        function (request, response) {
            db.todos.find(function (err, data) {
                if (err) {
                    handleError(err);
                } else {
                    response.json(data);
                }
            });
        });

    router.post("/todo",
        function (request, response) {
            db.todos.insert(request.body, function (err, data) {
                response.json(data);
            });
        });

    router.put('/todo/:_id',
        function (request, response) {
            db.todos.update(
                { _id: mongojs.ObjectId(request.params._id) },
                {
                    isCompleted: request.body.isCompleted,
                    text: request.body.text
                },
                {},
                function (err, data) {
                    response.json(data);
                });
        });

    router.get('/todo/clear',
        function (request, response) {
            db.todos.remove({},
                function (err, data) {
                    response.end();
                });
        });

    function handleError(err) {
        console.error("Something bad happened!", err);
    }

    module.exports = router;
})();