(function () {
	angular
		.module('todoApp')
		.config(function ($routeProvider, $locationProvider) {
			$routeProvider
				.when('/', {
				templateUrl: '/app/templates/todoOverview.html',
				controller: 'todoOverviewController',
				controllerAs: 'vm'
			})
				.when('/test', {
				templateUrl: '/app/templates/test.html',
			})
			.otherwise({
				redirectTo: '/'
			});

			$locationProvider.html5Mode(true);
		});
})();

