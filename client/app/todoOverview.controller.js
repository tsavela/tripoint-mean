(function () {
    'use strict';

    angular
        .module('todoApp')
        .controller('todoOverviewController', controller);

    function controller(todoService) {
        /* jshint validthis:true */
        var vm = this;
		
        vm.todos = {};
        
        vm.refreshTodoItems = refreshTodoItems;
        vm.createItem = createItem;

        activate();

        function activate() {
			refreshTodoItems();
		}
        
        function refreshTodoItems() {
            return todoService.getAll()
                .then(function(todoItems) {
                    vm.todos = todoItems;
                });
        }
        
        function createItem() {
            return todoService.create({ text: vm.newItemText })
                .then(function(createdItem) {
                    vm.todos.push(createdItem);
                    vm.newItemText = '';
                });
        }
    }
})();
