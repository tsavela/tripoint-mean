(function () {
    'use strict';

    angular
        .module('todoApp')
        .factory('todoService', todoService);

    function todoService($http) {
        var service = {
            getAll: getAll,
            update: update,
            create: create
        };

        return service;

        function getAll() {
            return $http.get('/api/todo')
                .then(function(response) {
                    return response.data;
                });
        }
        
        function update(todoItem) {
            return $http.put('/api/todo/' + todoItem._id, todoItem)
                .then(function(response) {
                    return response.data;
                });
        }
        
        function create(todoItem) {
            return $http.post('/api/todo', todoItem)
                .then(function(response) {
                    return response.data;
                });
        }
    }
})();