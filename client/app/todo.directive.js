(function() {
	'use strict';
	
	angular
		.module('todoApp')
		.directive('todo', todoDirective);
		
	function todoDirective(todoService) {
		var directive = {
            link: link,
            restrict: 'E',
			templateUrl: '/app/templates/todo.directive.template.html',
			scope: {
				todoItem: '='
			}
        };
        return directive;

        function link(scope, element, attrs) {
			scope.isCompletedChanged = function() {
				todoService.update(scope.todoItem);	
			};
        }
	}
})();