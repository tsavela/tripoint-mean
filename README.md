# README #

### What is this repository for? ###

* Source code from presentation at Tripoint.

### How do I get set up? ###

* Install node.js and MongoDB on your machine.
* Start MongoDB (see it's documentation on how to).
* Open console and do "npm install" in project's source folder.
* Start hosting with "npm start".